﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{
    Vector3 objectPos;

    private static move instance;

    private CharacterController controller;
    private Vector3 moveVector;

    private GameManager gm;
    private Rigidbody rb;
    private float speed;
    private float speedInc;
    public Vector3 playerPos = new Vector3(0, 1.5f, -120);
    

    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        instance = this;
        rb = GetComponent<Rigidbody>();

        controller = GetComponent<CharacterController>();

    }
    void FixedUpdate()
    {
        speed = gm.speed;
        speedInc = gm.speedInc;
        //transform.position = new Vector3 (Mathf.Clamp(gameObject.transform.position.x, -4.5f, 4.5f),1.5f, Mathf.Clamp(gameObject.transform.position.z, -10000000f, 10000000f));
        rb.velocity = new Vector3(0f,0f,1f) * speed * speedInc;
        //rb.AddForce(transform.forward * speed * speedInc);
        

    }
    void OnMouseDown()
    {
        objectPos = Camera.main.WorldToScreenPoint(transform.position);
    }
    void OnMouseDrag()
    {
        Vector3 pos = new Vector3(Input.mousePosition.x, objectPos.y, objectPos.z);
        transform.position = Camera.main.ScreenToWorldPoint(pos);
    }
}
