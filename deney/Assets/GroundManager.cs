﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundManager : MonoBehaviour
{
    public GameObject[] groundPrefabs;
    private Transform playerTransform;
    private float spawnZ = 0.0f;
    private float groundLenght = 100.0f;
    private int amnGroundOnScreen;
    private List<GameObject> activateGrounds;
    private bool ii = false;
    public GameObject endGround;


    void Start()
    {
        amnGroundOnScreen = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>().amnGroundOnScreen;
        activateGrounds = new List<GameObject>();
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        for (int i = 0; i < amnGroundOnScreen; i++)
        {
            SpawnGround();
            ii = true;
        }
        if (ii == true)
        {
            SpawnEndGround();
            ii = false;
        }
    }
    private void SpawnGround(int prefabIndex = -1)
    {

        GameObject go;
        go = Instantiate(groundPrefabs[Random.Range(0, groundPrefabs.Length)]) as GameObject;
        go.transform.SetParent(transform);
        go.transform.position = Vector3.forward * spawnZ;
        spawnZ += groundLenght;
        activateGrounds.Add(go);

    }
    private void SpawnEndGround()
    {
        GameObject end;
        end = Instantiate(endGround) as GameObject;
        end.transform.SetParent(transform);
        end.transform.position = Vector3.forward * spawnZ;
        spawnZ += groundLenght;
        activateGrounds.Add(end);
    }
}
